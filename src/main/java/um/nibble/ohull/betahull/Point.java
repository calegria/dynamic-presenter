/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.ohull.betahull;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import math.geom2d.Point2D;


/**
 * A mutable and property-enabled version of a {@link Point2D}.
 * 
 * @author Carlos Alegría
 */
public class Point extends Point2D  {
	
	private final BooleanProperty maximalProperty;
	private final DoubleProperty xProperty;
	private final DoubleProperty yProperty;
	
	/**
	 * @param x
	 * @param y
	 * @param maximal
	 */
	public Point(double x, double y, boolean maximal) {
		this.maximalProperty = new SimpleBooleanProperty(maximal);
		
		this.x = x;
		this.xProperty = new SimpleDoubleProperty(x);
		this.xProperty.addListener((ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) -> {
						this.x = newValue.doubleValue();
					});
		
		this.y = y;
		this.yProperty = new SimpleDoubleProperty(y);
		this.yProperty.addListener((ObservableValue<? extends Number> observable,
				Number oldValue, Number newValue) -> {
					this.y = newValue.doubleValue();
				});
	}
	
	/**
	 * 
	 */
	public Point() {
		this(0,0,false);
	}
	
	/**
	 * @param x
	 * @param y
	 */
	public Point(double x, double y) {
		this(x, y, false);
	}
	
	/**
	 * @param point
	 */
	public Point(Point point) {
		this(point.x, point.y, point.maximalProperty.get());
	}

	/**
	 * @param point
	 */
	public Point(Point2D point) {
		this(point.x(), point.y(), false);
	}

	public final boolean isMaximal() {
		return this.maximalProperty.get();
	}
	
	public final void setMaximal(boolean maximal) {
		this.maximalProperty.set(maximal);
	}
	
	public BooleanProperty maximalProperty() {
		return this.maximalProperty;
	}
	
	@Override
	public final double getX() {
		return this.xProperty.get();
	}
	
	public final void setX(double x) {
		this.xProperty.set(x); 
	}
	
	public DoubleProperty xProperty() {
		return this.xProperty;
	}
	
	@Override
	public final double getY() {
		return this.yProperty.get();
	}
	
	public final void setY(double y) {
		this.yProperty.set(y);
	}
	
	public DoubleProperty yProperty() {
		return this.yProperty;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Point(" + xProperty.get() + ", " + yProperty.get() + ", " + maximalProperty.get() + ")";
	}
}