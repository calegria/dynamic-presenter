/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.ohull.betahull;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import math.geom2d.line.LineSegment2D;
import math.geom2d.polygon.SimplePolygon2D;

/**
 * @author Carlos Alegría
 */
public class DynamicBetaHullModel {

	private static final double ANGLE_STEP = StrictMath.toRadians(1);
	
	//
	// interface properties / bindings
	//
	
	
	// the area of the O2Hull in units
	//
	private final DoubleProperty area;
	
	// the angle of the O2 set, in radians 
	//
	private final DoubleProperty angle;
	
	// Contains the equivalent in degrees of the current angle. Used to expose
	// it as an observable value.
	//
	private final DoubleBinding degAngle;

	// the list of overlaps of the O2Hull
	//
	private final ObservableList<Point[]> overlaps;
	
	// the list of components of the O2Hull
	//
	private final ObservableList<Point[]> components;	
	
	//
	// internal helper variables
	//

	
	// Contains the tangent of the current angle. Used to simplify operations
	// on the O2HUll computation
	//
	private final DoubleBinding tanAngle;
	
	// points container
	//
	private final LinkedList<Point> points;
	
	// Provides a total lexicographical order in a collection of Point objects,
	// by comparing the projection on X axis along variable axis. Ties are
	// solved using Y coordinate.
	//
	private final Comparator<Point> projectedComp;
	
	// used to trigger O2Hull computation when the coordinates of points change
	//
	private final ChangeListener<? super Number> changeListener;
	
	private final Logger logger = LoggerFactory.getLogger(DynamicBetaHullModel.class);

	/**
	 * 
	 */
	public DynamicBetaHullModel() {

		this.area = new SimpleDoubleProperty();
		this.components = FXCollections.observableArrayList();
		this.overlaps = FXCollections.observableArrayList();
		this.points = new LinkedList<>();
		
		//
		// angle
		//

		this.angle = new SimpleDoubleProperty();
		
		// angle changes trigger OHull computation
		//
		this.angle.addListener((
				ObservableValue<? extends Number> observable, Number oldValue,
				Number newValue) -> {
					logger.debug("orientation changed: {} -> {}",
							oldValue, newValue);
					computeOHull();
				});
		
		this.tanAngle = new DoubleBinding() {

			{
				super.bind(angle);
			}

			@Override
			protected double computeValue() {
				return StrictMath.tan(angle.get());
			}
		};
		this.degAngle = new DoubleBinding() {

			{
				super.bind(angle);
			}

			@Override
			protected double computeValue() {
				return StrictMath.toDegrees(angle.get());
			}
		};
		
		//
		// changes in points coordinates trigger OHull computation
		//
		
		this.changeListener = (ObservableValue<? extends Number> observable,
				Number oldValue, Number newValue) -> {
					logger.debug("point property {} changed: {} -> {}",
							new Object[] {observable, oldValue, newValue});
					computeOHull();
				};

		// Provides a total lexicographical order. Compares the projection on X
		// axis along variable axis. Ties are solved using Y coordinate
		//
		this.projectedComp = (Point p1, Point p2) -> {

			// project X coordinate along O-line
			//
			double p1X = p1.getX() - p1.getY() / this.tanAngle.get();
			double p2X = p2.getX() - p2.getY() / this.tanAngle.get();

			logger.debug("p1X = {}", p1X);
			logger.debug("p2X = {}", p2X);
			
			if (p1X > p2X) {
				return 1;
			}
			if (p1X < p2X) {
				return -1;
			}
			if (p1.getY() > p2.getY()) {
				return 1;
			}
			if (p1.getY() < p2.getY()) {
				return -1;
			}
			return 0;
		};
	}

	/**
	 * 
	 */
	private void computeOHull() {

		//
		// clean up OHull representation
		//
		
		this.components.clear();
		this.overlaps.clear();
		
		//
		// trivial hulls
		//

		switch (this.points.size()) {
		case 1:
			this.points.forEach(p -> p.setMaximal(true));
		case 0:
			return;
		}
		
		//
		// singular cases
		//
		
		if (this.angle.get() == 0 || this.angle.get() == StrictMath.PI) {
			this.points.forEach(p -> p.setMaximal(true));
			return;
		}
		
		this.points.forEach(p -> p.setMaximal(false));
		
		//
		// computing staircases
		//
		
		Collections.sort(this.points, projectedComp);
		logger.debug("points: {}", Arrays.toString(this.points.stream().toArray()));

		// first and fourth
		//
		LinkedList<Point> first = computeStaircase(this.points.descendingIterator(), true);
		LinkedList<Point> fourth = computeStaircase(this.points.descendingIterator(), false);

		// second and third
		//
		LinkedList<Point> second = computeStaircase(this.points.iterator(), true);
		LinkedList<Point> third = computeStaircase(this.points.iterator(), false);
		
		//
		// computing components and overlaps
		//
		
		Collections.reverse(third);
		logger.debug("first staircase: {}", Arrays.toString(first.stream().toArray()));
		logger.debug("third staircase: {}", Arrays.toString(third.stream().toArray()));
		
		LinkedList<Point> intersections = intersect(first.listIterator(), third.listIterator());
		if (!intersections.isEmpty()) {
			
			//
			// first and third staircases intersect
			//
			
			logger.debug("first-third intersections: {}",
					Arrays.toString(intersections.stream().toArray(Point[]::new)));
			constructComponents(first.listIterator(), third.listIterator(),
					intersections, fourth, second);
		} else {
			
			Collections.reverse(fourth);
			logger.debug("second staircase: {}", Arrays.toString(second.stream().toArray()));
			logger.debug("fourth staircase: {}", Arrays.toString(fourth.stream().toArray()));
			
			intersections = intersect(second.listIterator(), fourth.listIterator());
			if (!intersections.isEmpty()) {
				
				//
				// second and fourth staircases intersect
				//
				
				logger.debug("second-fourth intersections: {}",
						Arrays.toString(intersections.stream().toArray(Point[]::new)));
				Collections.reverse(third);
				constructComponents(second.listIterator(), fourth.listIterator(),
						intersections, third, first);
			} else {
		
				//
				// no intersections, there is a single component
				//
				
				LinkedList<Point> component = new LinkedList<>(third);
				for (Point point : second) {
					component.addLast(point);
				}
				for (Point point : fourth) {
					component.addFirst(point);
				}
				for (Point point : first) {
					component.addFirst(point);
				}

				Point arrComp[] = component.stream().toArray(Point[]::new);
				logger.debug("single component: {}", Arrays.toString(arrComp));
				this.components.add(arrComp);
			}
		}
		
		
		//
		// computing area
		//
		
		logger.debug("{} components", this.components.size());
		double area = 0;
		for (Point[] component : this.components) {
			area += StrictMath.abs(new SimplePolygon2D(component).area());
		}
		
		this.area.set(area);
	}

	/**
	 * @param right
	 * @param opposite
	 * @param intersections
	 * @param init
	 * @param fin
	 */
	private void constructComponents(ListIterator<Point> right,
			ListIterator<Point> opposite, List<Point> intersections,
			List<Point> init, List<Point> fin) {

		//
		// local variables
		//
		
		boolean overlap;
		Point intersection;
		LinkedList<Point> component = new LinkedList<>();
		Iterator<Point> intIt = intersections.iterator();

		//
		// initial component
		//
		
		if (init.isEmpty()) {
			overlap = true;
			component.add(intIt.next());
			
			ListIterator<Point> tmp = right;
			right = opposite;
			opposite = tmp;
		} else {
			overlap = false;
			component.addAll(init);
		}
		
		//
		// main cycle
		//
		
		while (intIt.hasNext()) {

			intersection = intIt.next();
			
			//
			// adding right staircase points
			//
			
			while (true) {
				Point rPoint = right.next();
				
				if (!rPoint.equals(component.peekFirst())) {
					component.addFirst(rPoint);
				}
				if (rPoint.getY() >= intersection.getY()) {
					break;
				}
			}

			//
			// adding opposite staircase points
			//
			
			while (true) {
				Point oppPoint = opposite.next();
				
				if (oppPoint.getY() >= intersection.getY()) {
					opposite.previous();
					break;
				}
				if (!oppPoint.equals(component.peekLast())) {
					component.addLast(oppPoint);
				}
			}

			// adding final intersection point
			//
			component.add(intersection);

			//
			// updating properties
			//

			Point arrComp[] = component.stream().toArray(Point[]::new);
			if (overlap) {
				logger.debug("overlap: {}", Arrays.toString(arrComp));
				this.overlaps.add(arrComp);
			} else {
				logger.debug("component: {}", Arrays.toString(arrComp));
				this.components.add(arrComp);
			}

			//
			// updating state
			//

			if (!intersection.isMaximal()) {

				ListIterator<Point> tmp = right;
				right = opposite;
				opposite = tmp;

				overlap = !overlap;
			}
			
			//
			// initializing next component
			//
			
			component.clear();
			component.add(intersection);
		};

		//
		// final component
		//
		
		if (!fin.isEmpty()) {
		
			//
			// adding right staircase points
			//
			
			while (right.hasNext()) {
				Point rPoint = right.next();
				if (right.hasNext()) {
					component.addFirst(rPoint);
				}
			}

			//
			// adding opposite staircase points
			//
			
			while (opposite.hasNext()) {
				Point oppPoint = opposite.next();
				if (opposite.hasNext()) {
					component.addLast(oppPoint);
				}
			}
			
			component.addAll(fin);
			
			//
			//
			//
			
			Point arrComp[] = component.stream().toArray(Point[]::new);
			logger.debug("final component: {}", Arrays.toString(arrComp));
			this.components.add(arrComp);
		}
	}
	
	/**
	 * @param points
	 * @param positive
	 * @return
	 */
	private LinkedList<Point> computeStaircase(Iterator<Point> points,
			boolean positive) {

		//
		// selecting maximal elements
		//

		LinkedList<Point> result = new LinkedList<>();

		{
			//
			// first element is maximal
			//

			Point oPoint = points.next();
			oPoint.setMaximal(true);
			result.add(new Point(oPoint));

			//
			// traversing the rest of the set
			//

			while (points.hasNext()) {
				oPoint = points.next();

				if (positive
						? oPoint.getY() > result.getLast().getY()
						: oPoint.getY() < result.getLast().getY()) {
					oPoint.setMaximal(true);
					result.add(new Point(oPoint));
				}
			}
		}

		//
		// trivial staircase (no corners)
		//
		
		if (result.size() < 2) {
			result.clear();
			return result;
		}

		//
		// creating staircase
		//
		
		ListIterator<Point> it = result.listIterator();
		Point current = null;
		Point next = it.next();
		
		do {
			Point corner = new Point();
			
			//
			// inserting the corner of the step in the middle of every pair of
			// consecutive maximal elements
			//
			
			current = next;
			it.add(corner);
			next = it.next();
			
			//
			// updating coordinates, avoid rolling back the cursor 
			//
			
			double ca = StrictMath.abs(next.getY() - current.getY())
					/ this.tanAngle.get();
			logger.debug("ca = {}", ca);
			
			double x = positive ? next.getX() - ca : next.getX() + ca;
			logger.debug("x = {}", x);
			corner.setX(x);
			corner.setY(current.getY());
			
		} while (it.hasNext());
		return result;
	}
	
	/**
	 * @param right
	 * @param opposite
	 * @return
	 */
	private LinkedList<Point> intersect(ListIterator<Point> right,
			ListIterator<Point> opposite) {

		// intersection list
		//
		LinkedList<Point> result = new LinkedList<>();

		// trivial case
		//
		if (!right.hasNext() || !opposite.hasNext()) return result;

		//
		// state variables
		//
		
		LineSegment2D opSegment = null;
		
		Point rStart = null;
		Point rEnd = null;
		
		Point opStart = null;
		Point opEnd = null;

		ListUpdate listUpdate = null;
		
		//
		// initial conditions
		//

		rStart = right.next();
		opEnd = opposite.next();
		listUpdate = ListUpdate.SKIP_RIGHT; // done in both maximal and no intersections

		if (rStart.equals(opEnd)) { // initial maximal intersection

			// adding intersection
			//
			Point p = new Point(rStart);
			result.add(p);
			
			// swapping staircases
			//
			ListIterator<Point> tmpIt = right;
			right = opposite;
			opposite = tmpIt;
			
			logger.debug("maximal leaving {}, swap and skip right", p);
			
		} else { // no intersection
			
			// roll back right staircase, to reflect initial state
			//
			right.previous();
		}
		
		//
		// main cycle
		//

		MAIN_CYCLE:
		while (true) {
			
			//
			// setting up state variables
			//

			switch (listUpdate) {
			case SWAP:
				
				logger.debug("swap");
				
				// swapping staircases
				//
				ListIterator<Point> tmpIt = right;
				right = opposite;
				opposite = tmpIt;
				
				// swapping points
				//
				rStart = opEnd;
				opEnd = rEnd;
				break;
				
			case SKIP_RIGHT:
				
				logger.debug("skip right");
				
				if (!right.hasNext()) break MAIN_CYCLE;
				rStart = right.next();
				break;
				
			case SKIP_OPPOSITE:
				
				logger.debug("skip opposite");
				
				if (!opposite.hasNext()) break MAIN_CYCLE;
				opStart = opEnd;
				opEnd = opposite.next();
				break;
			}
			
			//
			// detecting opposite skipping
			//
			
			if (rStart.getY() > opEnd.getY()) {
				listUpdate = ListUpdate.SKIP_OPPOSITE;
				opSegment = null;
				continue;
			}

			//
			// detecting intersections
			//
			
			if (!right.hasNext()) break MAIN_CYCLE;
			rEnd = right.next();
			
			listUpdate = ListUpdate.SKIP_RIGHT;
			if (rEnd.equals(opEnd)) { // segments reaching a maximal
				
				Point p = new Point(rEnd);
				result.add(p);
				logger.debug("maximal reaching {}, skip right", p);
				
			} else {

				// creating opSegment if required
				//
				if (opSegment == null) {
					opSegment = new LineSegment2D(opStart, opEnd);
				}

				LineSegment2D rSegment = new LineSegment2D(rStart, rEnd);

				if (LineSegment2D.intersects(rSegment, opSegment)) {
					
					// LineSegment2D.intersection does not handle V
					// intersections correctly
					//
					Point p = new Point(rSegment.intersection(opSegment));
					
					result.add(p);
					listUpdate = ListUpdate.SWAP;
					opSegment = null;
					
					logger.debug("proper {}, swap", p);
				}
			}
		};
		return result;
	}

	/**
	 * @param point
	 */
	public void addPoint(Point point) {
		
		if (!this.points.add(point)) return;
		
		// point coordinates modification triggers OHull calculation
		//
		point.xProperty().addListener(this.changeListener);
		point.yProperty().addListener(this.changeListener);

		// points set modification triggers OHull calculation
		//
		computeOHull();
	}
	
	public void addAll(Point ... points) {
		for (Point point : points) {
			if (!this.points.add(point)) continue;
			point.xProperty().addListener(this.changeListener);
			point.yProperty().addListener(this.changeListener);
		}
		computeOHull();
	}

	/**
	 * @param point
	 */
	public void removePoint(Point point) {
		
		int indexOf = this.points.indexOf(point);
		if (indexOf == -1) return;
		
		// coordinates listener removal
		//
		Point removedPoint = this.points.remove(indexOf);
		removedPoint.xProperty().removeListener(this.changeListener);
		removedPoint.yProperty().removeListener(this.changeListener);
		
		// points set modification triggers OHull calculation
		//
		computeOHull();
	}
	
	/**
	 * 
	 */
	public void clear() {
		this.points.stream().forEach(point -> {
			point.xProperty().removeListener(this.changeListener);
			point.yProperty().removeListener(this.changeListener);
		});
		this.points.clear();
		computeOHull();
	}
	
	/**
	 * @return
	 */
	public ObservableList<Point[]> getComponentsList() {
		return this.components;
	}
	
	/**
	 * @return
	 */
	public ObservableList<Point[]> getOverlapsList() {
		return this.overlaps;
	}

	/**
	 * @return
	 */
	public DoubleProperty angleProperty() {
		return angle;
	}
	
	/**
	 * @param angle
	 */
	public final void setAngle(double angle) {
		// limit angle to [0,PI]
		//
		angle = angle < 0 ? 0 : angle > StrictMath.PI ? StrictMath.PI : angle;
		
		// avoid O2Hull computation by setting the same angle
		//
		if (angle == this.angle.get()) return;
		
		this.angle.set(angle);
	}
	
	/**
	 * @return
	 */
	public final double getAngle() {
		return this.angle.get();
	}
	
	/**
	 * @param direction
	 */
	public void stepAngle(boolean direction) {
		setAngle(this.angle.get() + (direction ? ANGLE_STEP : -ANGLE_STEP));
	}
	
	/**
	 * @return
	 */
	public ObservableValue<Number> getDegAngle() {
		return this.degAngle;
	}

	/**
	 * @return
	 */
	public DoubleProperty areaProperty() {
		return area;
	}
	
	/**
	 * @author Carlos Alegría
	 *
	 */
	private static enum ListUpdate {
		SWAP,
		SKIP_RIGHT,
		SKIP_OPPOSITE;
	}
}