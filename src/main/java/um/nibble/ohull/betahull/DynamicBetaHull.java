/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.ohull.betahull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.binding.IntegerExpression;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Region;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.util.Pair;
import um.nibble.util.UnicodeChars;

/**
 * @author Carlos Alegría
 *
 */
public class DynamicBetaHull extends Region {

	private static final String MAXIMAL_CLASS = "point_maximal";
	private static final String NORMAL_CLASS = "point_normal";
	private static final String POINT_CLASS = "point";
	private static final String OHULL_COMPONENT_CLASS = "ohull_component";
	private static final String OHULL_OVERLAP_CLASS = "ohull_overlap";
	private static final String ORIENTATION_LABEL_ID = "orientation_label";
	private static final String AREA_LABEL_ID = "area_label";

	// hires
	//
	private static final double RADIUS = 7.4;

	// 800x600
	//
//	private static final double RADIUS = 5;

	// 1024x768
	//
//	private static final double RADIUS = 5;

	private static final KeyCombination AREA_TOGGLE = KeyCombination.valueOf("ctrl+a");
	private static final KeyCombination ANGLE_TOGGLE = KeyCombination.valueOf("ctrl+b");
	private static final KeyCombination PRINT_POINTS = KeyCombination.valueOf("ctrl+p");

	private final Group components;
	private final Group overlaps;
	private final Text angleLabel;
	private final Text areaLabel;

	private final DynamicBetaHullModel model;

	private final Logger logger = LoggerFactory.getLogger(DynamicBetaHull.class);

	/**
	 * Viewer -> Model communication is done through point addition Model -> Viewer
	 * communication is done through orientation and staircases properties binding
	 */
	public DynamicBetaHull() {
		super();

		//
		// ohull shapes
		//

		this.components = new Group();
		getChildren().add(this.components);

		this.overlaps = new Group();
		getChildren().add(this.overlaps);

		//
		// setting up model
		//

		this.model = new DynamicBetaHullModel();
		this.model.getComponentsList().addListener((ListChangeListener.Change<? extends Point[]> c) -> {
			while (c.next()) {
				logger.debug("components list modified: {}", c);

				if (c.wasRemoved()) {
					components.getChildren().clear();
				}
				if (c.wasAdded()) {
					ObservableList<Node> list = components.getChildren();

					for (Point[] component : c.getAddedSubList()) {

						Polygon p = new Polygon();
						p.getStyleClass().add(OHULL_COMPONENT_CLASS);
						ObservableList<Double> pPoints = p.getPoints();

						for (Point point : component) {
							pPoints.addAll(point.getX(), -point.getY());
						}

						list.add(p);
					}
				}
			}
		});
		this.model.getOverlapsList().addListener((ListChangeListener.Change<? extends Point[]> c) -> {
			while (c.next()) {
				logger.debug("overlaps list modified: {}", c);

				if (c.wasRemoved()) {
					overlaps.getChildren().clear();
				}
				if (c.wasAdded()) {
					ObservableList<Node> list = overlaps.getChildren();

					for (Point[] component : c.getAddedSubList()) {

						Polygon p = new Polygon();
						p.getStyleClass().add(OHULL_OVERLAP_CLASS);
						ObservableList<Double> pPoints = p.getPoints();

						for (Point point : component) {
							pPoints.addAll(point.getX(), -point.getY());
						}

						list.add(p);
					}
				}
			}
		});
		this.model.setAngle(StrictMath.PI / 2);

		//
		// beta label
		//
		// binding to model angle to automate value update
		//

		this.angleLabel = new Text();
		this.angleLabel.setId(ORIENTATION_LABEL_ID);
		this.angleLabel.textProperty()
				.bind(new SimpleStringProperty(UnicodeChars.BETA + " = ")
						.concat(Bindings.convert(IntegerBinding.integerExpression(this.model.getDegAngle()))
								.concat(new SimpleStringProperty(UnicodeChars.DEGREES))));
		getChildren().add(angleLabel);

		//
		// area label
		//
		// binding to model area to automate value update
		//

		this.areaLabel = new Text();
		this.areaLabel.setId(AREA_LABEL_ID);
		this.areaLabel.textProperty()
				.bind(new SimpleStringProperty("A = ")
						.concat(Bindings.format("%,d", IntegerExpression.integerExpression(this.model.areaProperty())))
						.concat(new SimpleStringProperty("u" + UnicodeChars.TWO_SUPERSCRIPT)));
		getChildren().add(areaLabel);

		//
		// points
		//
		// adding points with mouse (removal is done as a listener on every
		// circle)
		//

		setOnMousePressed((final MouseEvent event) -> {
			if (event.getButton() != MouseButton.PRIMARY) {
				return;
			}

			Pair<Node, Point> point = createPoint(event.getX(), event.getY());
			getChildren().add(point.getKey());
			this.model.addPoint(point.getValue());
		});

		//
		// scrolling changes orientation
		//

		this.setOnScroll((final ScrollEvent event) -> {
			this.model.stepAngle(event.getDeltaY() > 0);
		});

		//
		// key events configure visualization
		//

		setOnKeyPressed((final KeyEvent event) -> {

			// toggle angle and area label visibility
			//
			if (ANGLE_TOGGLE.match(event)) {
				this.angleLabel.setVisible(!this.angleLabel.isVisible());
			} else if (AREA_TOGGLE.match(event)) {
				this.areaLabel.setVisible(!this.areaLabel.isVisible());
			} else if (PRINT_POINTS.match(event)) {
				try {
//					Path tmp = Files.createTempFile("betahull_", ".txt");

//					try (PrintStream out = new PrintStream(Files.newOutputStream(tmp))) {

					logger.info("Dumping BetaHull status:");
					System.out.print(this.angleLabel.isVisible());
					System.out.print(" ");
					System.out.print(this.areaLabel.isVisible());
					System.out.print(" ");
					System.out.print(this.model.getAngle());
					System.out.print(" ");

					getChildren().stream().forEach(node -> {
						if (node == this.components || node == this.overlaps || node == this.angleLabel
								|| node == this.areaLabel) {
							return;
						}

						if (node instanceof Group) {
							Circle circle = (Circle) ((Group) node).getChildren().stream().findFirst().get();
							System.out.print(circle.getCenterX() + circle.getTranslateX());
							System.out.print(" ");
							System.out.print(circle.getCenterY() + circle.getTranslateY());
							System.out.print(" ");
						}
					});

					System.out.println();
					logger.info("Done.");
//					logger.info("BetaHull config saved to {}", tmp.toString());
//					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				return;
			}

			event.consume();
		});

		//
		// enabling key events
		//

		setFocusTraversable(true);
	}

	/**
	 * @param x
	 * @param y
	 * @return
	 */
	private Pair<Node, Point> createPoint(double x, double y) {

		final DragContext context = new DragContext();
		final Circle circle = new Circle(x, y, RADIUS);
		final Node wrap = new Group(circle);

		circle.getStyleClass().addAll(POINT_CLASS, NORMAL_CLASS);

		//
		// dragging and deletion
		//

		wrap.addEventFilter(MouseEvent.MOUSE_PRESSED, (final MouseEvent event) -> {

			switch (event.getButton()) {
			case PRIMARY: // start dragging circle

				context.anchorX = event.getX();
				context.anchorY = event.getY();
				context.translateX = circle.getTranslateX();
				context.translateY = circle.getTranslateY();
				break;

			case SECONDARY: // destroying circle and point

				getChildren().remove(wrap);
				model.removePoint((Point) circle.getUserData());
				break;

			default:
				return;
			}
			event.consume();
		});
		wrap.addEventFilter(MouseEvent.MOUSE_DRAGGED, (final MouseEvent event) -> {

			final double translateX = context.translateX + event.getX() - context.anchorX;
			final double translateY = context.translateY + event.getY() - context.anchorY;

			// translate to new position if it is greater than circle
			// radius and does not exceeds borders of the pane
			//
			circle.setTranslateX(translateX);
			circle.setTranslateY(translateY);
		});

		//
		// setup mirroring point in model
		//

		Point point = new Point(circle.getCenterX(), -circle.getCenterY());

		// point maximality defines circle CSS style
		//
		point.maximalProperty()
				.addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
					if (oldValue == newValue) {
						return;
					}
					if (newValue) {
						circle.getStyleClass().remove(NORMAL_CLASS);
						circle.getStyleClass().add(MAXIMAL_CLASS);
					} else {
						circle.getStyleClass().remove(MAXIMAL_CLASS);
						circle.getStyleClass().add(NORMAL_CLASS);
					}
				});

		// circle translation changes point x/y values
		//
		point.xProperty().bind(circle.centerXProperty().add(circle.translateXProperty()));
		point.yProperty().bind(circle.centerYProperty().add(circle.translateYProperty()).negate());

		circle.setUserData(point);

		return new Pair<>(wrap, point);
	}

	/**
	 * @return
	 */
	public DynamicBetaHullModel getModel() {
		return this.model;
	}

	/**
	 * @param visible
	 */
	public void setAreaLabelVisible(boolean visible) {
		this.areaLabel.setVisible(visible);
	}

	/**
	 * @param visible
	 */
	public void setAngleLabelVisible(boolean visible) {
		this.angleLabel.setVisible(visible);
	}

	/**
	 * @param points
	 */
	public void load(float... coords) {

		//
		// clear everything
		//

		this.model.clear();
		getChildren().clear();
		getChildren().addAll(this.components, this.overlaps, this.angleLabel, this.areaLabel);

		//
		// add points
		//

		if (coords.length != 0) {
			Node nodes[] = new Node[coords.length / 2];
			Point points[] = new Point[coords.length / 2];
			for (int i = 0, j = 0; i < coords.length;) {
				Pair<Node, Point> pair = createPoint(coords[i++], coords[i++]);
				nodes[j] = pair.getKey();
				points[j++] = pair.getValue();
			}

			getChildren().addAll(nodes);
			this.model.addAll(points);
		}
	}

	/**
	 * @author Carlos Alegría
	 *
	 */
	private static final class DragContext {
		public double anchorX;
		public double anchorY;
		public double translateX;
		public double translateY;

		@Override
		public String toString() {
			return "(" + anchorX + ", " + anchorY + ", " + translateX + ", " + translateY + ")";
		}
	}
}