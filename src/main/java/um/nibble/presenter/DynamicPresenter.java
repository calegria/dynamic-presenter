/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.presenter;

import java.io.InputStream;
import java.util.logging.LogManager;

import javafx.application.Application;
import um.nibble.presenter.gui.JFXApplication;

/**
 * @author Carlos Alegría
 *
 */
public class DynamicPresenter {
	
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		//
		// check arguments
		//
		
		if (args.length < 2) {
			throw new Exception("Missing presentation specification argument");
		}
		
		//
		// configuring logging system
		//
		
		try (InputStream in =
				ClassLoader.getSystemResourceAsStream("logging.properties")) {
			LogManager.getLogManager().readConfiguration(in);
		}
		
		//
		// executing application
		//
		
		Application.launch(JFXApplication.class, args);
	}
}