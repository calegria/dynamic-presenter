/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.presenter.gui;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.lang3.ArrayUtils;
import org.jpedal.PdfDecoderFX;
import org.jpedal.display.Display;
import org.jpedal.exception.PdfException;
import org.jpedal.parser.DecoderOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import um.nibble.ohull.betahull.DynamicBetaHull;
import um.nibble.util.UnicodeChars;

/**
 * @author calegria
 *
 */
public class JFXApplication extends Application {

	private static final String PROPERTIES_FILE = "app.properties";
	private static final String BETA_HULL_CSS_FILE = "betahull.css";
	private static final String RESOURCES_ICONS = "resource.icons";
	private static final String RESOURCES_KC_FULL_SCREEN = "input.combination.fullscreen";
	private static final String RESOURCES_KC_NEXT_SLIDE = "input.combination.next_slide";
	private static final String RESOURCES_KC_PREVIOUS_SLIDE = "input.combination.previous_slide";
	private static final String WINDOW_TITLE = "Dynamic O" + UnicodeChars.BETA_SUBSCRIPT + "-hull";
	
	private static final Res res = Res.RES_1024x768;
	private static final float RES_800_x_600_X_SCALE = 800f / 1280f;
	private static final float RES_800_x_600_Y_SCALE = 600f / 800f;
	private static final float RES_1024_x_768_X_SCALE = 1024f / 1280f;
	private static final float RES_1024_x_768_Y_SCALE = 768f / 800f;
	private static final float RES_1024_x_768 = 1.1f;
	private static final float RES_800_x_600 = 1f;
	private static final float RES_1280_x_800 = 1.35f;
	
	private static final int PARAMETER_PRESENTATION = 0;
	private static final int PARAMETER_PDF_FILE = 1;
	
	private final Logger logger = LoggerFactory.getLogger(JFXApplication.class);
	private PdfDecoderFX pdf;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {

		//
		// Internal properties
		//
		
		final Properties properties = new Properties();
		try (InputStream in =
				ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE)) {
			properties.load(in);
		}
		
		//
		// key combinations
		//
			
		final KeyCombination fullScreen = KeyCombination.valueOf(
				properties.getProperty(RESOURCES_KC_FULL_SCREEN, null));
		//
		// icons
		//
			
		Image icons[];
		{
			String[] iconNames = properties.getProperty(RESOURCES_ICONS, null).split(",");
			icons = new Image[iconNames.length];
			for (int i = 0; i < iconNames.length; i++) {
				icons[i] = new Image(
						ClassLoader.getSystemResourceAsStream(iconNames[i].trim()));
			}
		}		
		
		//
		// setting up stage
		//
		
		stage.setScene(createScene(properties));
		stage.setTitle(WINDOW_TITLE);
		stage.getIcons().addAll(icons);
		stage.setFullScreen(true);
		stage.fullScreenProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue) {
				stage.setMaximized(true);
			}
		});
		stage.addEventFilter(KeyEvent.KEY_PRESSED, (final KeyEvent event) -> {
			if (fullScreen.match(event) && !stage.isFullScreen()) {
				stage.setFullScreen(true);
				event.consume();
			}
		});
		
		stage.show();
	}
	
	/**
	 * @return
	 */
	private Scene createScene(Properties properties) {

		//
		// slides list
		//
		
		final List<Pair<SlideType, Object>> slides = new ArrayList<>();
		{
			Path path = FileSystems.getDefault().getPath(
					getParameters().getUnnamed().get(PARAMETER_PRESENTATION));

			try (Scanner scanner = new Scanner(
					path, StandardCharsets.UTF_8.name())) {
				while (scanner.hasNext()) {
					switch (SlideType.valueOf(scanner.next())) {
					case BETA_HULL:
						
						BetaHullConfig config = new BetaHullConfig();
						
						// labels and angle
						//
						config.angleLabelVisible = scanner.nextBoolean();
						config.areaLabelVisible = scanner.nextBoolean();
						config.angle = scanner.nextDouble();
						
						// points coordinates (if any)
						//
						List<Float> coords = new ArrayList<>(20);
						while (scanner.hasNextFloat()) {
							coords.add(scanner.nextFloat());
						}
						config.coords = ArrayUtils.toPrimitive(coords.toArray(new Float[0]));
						
						if (res == Res.RES_800x600) {
							for (int i = 0; i < config.coords.length; i++) {
								config.coords[i++] *= RES_800_x_600_X_SCALE;
								config.coords[i] *= RES_800_x_600_Y_SCALE;
							}
						} else if (res == Res.RES_1024x768) {
							for (int i = 0; i < config.coords.length; i++) {
								config.coords[i++] *= RES_1024_x_768_X_SCALE;
								config.coords[i] *= RES_1024_x_768_Y_SCALE;
							}
						}
						
						// add slide
						//
						slides.add(new Pair<>(SlideType.BETA_HULL, config));
						break;
					case PDF:
						slides.add(new Pair<>(SlideType.PDF, scanner.nextInt()));
						break;
					}
				}
			} catch (IOException ioe) {
				throw new RuntimeException("Could not read presentation", ioe);
			}
		}

		if (slides.isEmpty()) {
			throw new RuntimeException("Empty presentation");
		}
		
		//
		// current slide
		//
		
		final ObjectProperty<Pair<SlideType, Object>> currentSlide = new SimpleObjectProperty<>();
		final ListIterator<Pair<SlideType, Object>> it = slides.listIterator();
		currentSlide.set(it.next());
		
		//
		// pdf decoder
		//
		
		pdf = new PdfDecoderFX();
		try {
//			Border redBorder = new Border(new BorderStroke(
//					Color.RED, null, null, null,
//					BorderStrokeStyle.SOLID, null, null, null,
//					null, new BorderWidths(5), Insets.EMPTY));
//			pdf.setBorder(redBorder);
			
			DecoderOptions.embedWidthData = true;
			DecoderOptions.showErrorMessages = true;
			pdf.openPdfFile(getParameters().getUnnamed().get(PARAMETER_PDF_FILE));
			pdf.setDisplayView(Display.SINGLE_PAGE, Display.DISPLAY_CENTERED);
			pdf.setBorderPresent(false);
			pdf.useHiResScreenDisplay(true);
			pdf.useNewGraphicsMode();
			setupPage(pdf, 1);
		} catch (PdfException pdfe) {
			throw new RuntimeException(pdfe);
		}
		
		pdf.visibleProperty().bind(new BooleanBinding() {

			{
				super.bind(currentSlide);
			}
			
			@Override
			protected boolean computeValue() {
				return currentSlide.get().getKey() == SlideType.PDF;
			}
		});
		
		currentSlide.addListener((observable, oldValue, newValue) -> {
			if (newValue.getKey() == SlideType.PDF) {
				setupPage(pdf, (Integer) newValue.getValue());
			}
		});
		
		//
		// dynamic beta hull
		//
		
		DynamicBetaHull betaHull = new DynamicBetaHull();
		if (currentSlide.getValue().getKey() == SlideType.BETA_HULL) {
			setupBetaHull(betaHull,
					(BetaHullConfig) currentSlide.getValue().getValue());
		}
		betaHull.visibleProperty().bind(new BooleanBinding() {

			{
				super.bind(currentSlide);
			}
			
			@Override
			protected boolean computeValue() {
				return currentSlide.get().getKey() == SlideType.BETA_HULL;
			}
		});
		
		currentSlide.addListener((observable, oldValue, newValue) -> {
			if (newValue.getKey() == SlideType.BETA_HULL) {
				setupBetaHull(betaHull, (BetaHullConfig) newValue.getValue());
			}
		});
		
		//
		// key combinations
		//
		
		final KeyCombination nextSlide = KeyCombination.valueOf(
				properties.getProperty(RESOURCES_KC_NEXT_SLIDE, null));
		final KeyCombination previousSlide =KeyCombination.valueOf(
				properties.getProperty(RESOURCES_KC_PREVIOUS_SLIDE, null));
		
		//
		// creating scene
		//
		
		Scene scene = new Scene(new StackPane(pdf, betaHull));
		scene.getStylesheets().add(
				ClassLoader.getSystemResource(BETA_HULL_CSS_FILE).toExternalForm());
		scene.setOnKeyPressed((final KeyEvent event) -> {
			if (nextSlide.match(event)) {
				if (it.hasNext()) {
					Pair<SlideType, Object> next = it.next();
					if (currentSlide.get() == next) {
						if (it.hasNext()) {
							currentSlide.set(it.next());
						}
					} else {
						currentSlide.set(next);
					}
				}
			} else if (previousSlide.match(event)) {
				if (it.hasPrevious()) {
					Pair<SlideType, Object> previous = it.previous();
					if (currentSlide.get() == previous) {
						if (it.hasPrevious()) {
							currentSlide.set(it.previous());
						}
					} else {
						currentSlide.set(previous);
					}
				}
			} else {
				return;
			}
			
			event.consume();
		});

		scene.setOnMousePressed(event -> {
			if (event.getButton() == MouseButton.MIDDLE) {
				if (it.hasNext()) {
					Pair<SlideType, Object> next = it.next();
					if (currentSlide.get() == next) {
						if (it.hasNext()) {
							currentSlide.set(it.next());
						}
					} else {
						currentSlide.set(next);
					}
				}
			}
		});
		
		return scene;
	}
	
	@Override
	public void stop() throws Exception {
		pdf.closePdfFile();
	}

	/**
	 * @param pdf
	 * @param page
	 */
	private void setupPage(PdfDecoderFX pdf, int page) {
		switch (res) {
		case RES_800x600:
			pdf.setPageParameters(RES_800_x_600, page);
			break;
		case RES_1280x800:
			pdf.setPageParameters(RES_1280_x_800, page);
			break;
		case RES_1024x768:
			pdf.setPageParameters(RES_1024_x_768, page);
			break;
		}
		
		pdf.decodePage(page);
		pdf.waitForDecodingToFinish();
		pdf.setMaxSize(pdf.getWidth(), pdf.getHeight());
		logger.debug("decode report for {} = {}", page, pdf.getPageDecodeReport());
	}
	
	/**
	 * @param hull
	 * @param config
	 */
	private void setupBetaHull(DynamicBetaHull hull, BetaHullConfig config) {
		hull.setAngleLabelVisible(config.angleLabelVisible);
		hull.setAreaLabelVisible(config.areaLabelVisible);
		hull.getModel().setAngle(config.angle);
		hull.load(config.coords);
	}
	
	private static enum Res { RES_1280x800, RES_800x600, RES_1024x768 };
	private static enum SlideType { BETA_HULL, PDF; }
	
	private final class BetaHullConfig {
		boolean angleLabelVisible;
		boolean areaLabelVisible;
		double angle;
		float coords[];
	}
}
