/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */
package um.nibble.util;

/**
 * @author calegria
 *
 */
final public class UnicodeChars {
	
	public static final String BETA = "\u03B2";
	public static final String BETA_SUBSCRIPT = "\u1D66";
	public static final String DEGREES = "\u00B0";
	public static final String TWO_SUPERSCRIPT = "\u00B2";

	private UnicodeChars() {
	}
}
