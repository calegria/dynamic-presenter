/* 
 * This file is part of Dynamic Presenter.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Dynamic Presenter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.
 *
 * Dynamic Presenter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynamic Presenter. If not, see <http://www.gnu.org/licenses/>.
 */package um.nibble.presenter.test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.pdfbox.PDFReader;
import org.apache.pdfbox.pdfviewer.PDFPagePanel;
import org.apache.pdfbox.pdfviewer.PageWrapper;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author calegria
 *
 */
public class SwingFx extends Application {

	@Override
    public void start (Stage stage) throws IOException {
        final SwingNode swingNode = new SwingNode();

        createSwingContent(swingNode);

        StackPane pane = new StackPane();
        pane.getChildren().add(swingNode);

        stage.setTitle("Swing in JavaFX");
        stage.setScene(new Scene(pane, 250, 150));
        stage.show();
    }

    private void createSwingContent(final SwingNode swingNode) throws IOException {
    	
        SwingUtilities.invokeLater(() -> {
        	try {
//        		PDFReader reader = new PDFReader();
//        		PageWrapper wrapper = new PageWrapper(reader);
        		
//        		Border redBorder = new Border(new BorderStroke(
//    					Color.RED, null, null, null,
//    					BorderStrokeStyle.SOLID, null, null, null,
//    					null, new BorderWidths(5), Insets.EMPTY));
//    			pdf.setBorder(redBorder);
        		PDDocument document = PDDocument.load(new File(
        				"/home/calegria/Documents/academico/articulos/o_convexidad/oriented_beta_hull/egc_2015/presentacion/presentacion-pdfjam.pdf"));
        		List<PDPage> pages = document.getDocumentCatalog().getAllPages();
				PDFPagePanel panel = new PDFPagePanel();
				panel.setPage(pages.get(0));
				panel.setPreferredSize(panel.getSize());
				panel.setBorder(javax.swing.border.LineBorder.createBlackLineBorder());
				panel.validate();
				
//				PDFReader reader = new PDFReader();
//        		PageWrapper wrapper = new PageWrapper(reader);
	            swingNode.setContent(panel);
//	            wrapper.displayPage(pages.get(0));
	            
//	            document.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
        });
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
    	launch(args);
    }
}
